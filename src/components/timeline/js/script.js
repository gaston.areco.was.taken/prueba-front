import * as vis from './vis';
import * as moment from './moment';

var container = {}
var items = {}
var options = {}
var timeline = {}

export default {
  name: "Timeline",
  mounted: function () {
    container = document.getElementById('visualization');

    items = new vis.DataSet();

    items.clear();

    for(var i = 0; i < this.$store.state.reservations.length; i++) {

      var tempItem = {
        id: i+1,
        start: this.$store.state.reservations[i].date + " " + this.$store.state.reservations[i].startTime,
        end: this.$store.state.reservations[i].date + " " + this.$store.state.reservations[i].endTime,
        className: this.$store.state.reservations[i].className
      }

      items.add(tempItem)

    }

    console.log(this.$store.state.reservations);

    options = {
      //stack: false,
      orientation: {
        axis: "top",
        item: "top"
      },
      //zoomMax: 31536000000, // just one year
      zoomMax: 87600900, // 10,000 years is maximum possible
      zoomMin: 10000000, // 10ms
      timeAxis: {scale: 'minute', step: 15},
      format: {
        minorLabels: function (date) {

          if (moment.duration(date.format("HH:mm")).asMinutes() % 60 === 0) {

            return date.format("H a");

          } else {

            return (moment.duration(date.format("HH:mm")).asMinutes() % 60) + " min.";

          }
        },

        majorLabels: function () { return "" }

      }
    };

    timeline = new vis.Timeline(container, items, options);

    function move(percentage) {
      var range = timeline.getWindow();
      var interval = range.end - range.start;
      timeline.setWindow({
        start: range.start.valueOf() - interval * percentage,
        end: range.end.valueOf() - interval * percentage
      });
    }

// attach events to the navigation buttons
    document.getElementById("moveLeft").onclick = function() {
      move(0.2);
    };
    document.getElementById("moveRight").onclick = function() {
      move(-0.2);
    };

// Using slider to zoomIn or zoomOut
    document.getElementById("sliderZoom").addEventListener("input", function() {
      var value = this.value;
      if (value < 0) {
        var start = moment().year(moment().year() - 100000), // to adjust with options
            end = moment().year(moment().year() + 1);
        timeline.zoomOut(-value);
        if (value === "-1") timeline.setWindow(start, end);
      } else if (value > 0) {
        //var start = moment(), end = moment(moment().utc() + 10);
        timeline.zoomIn(value);
        if (value === "1") timeline.setWindow(start, end);
      } else {
        timeline.fit(items.getIds());
        this.value = 0;
      }
    });

// To reset zoom initial state
    document.getElementById("fit").onclick = function() {
      //$('.range').next().text('0'); // set default if to use output with input range
      document.getElementById("sliderZoom").value = 0;
      timeline.fit(items.getIds());
    };

  }
}
