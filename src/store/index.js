import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        reservations: [
            { name: 'Alvarado, Eduardo Alfonso', date:'2020-10-14', startTime: '12:00', endTime: '13:00', className: "yellow" },
            { name: 'Acuña Lopez, Juliana', date:'2020-10-14', startTime: '13:30', endTime: '14:45', className: "yellow" }
        ]
    },
    getters: {
        getReservations(state) {
            return state.reservations
        }
    },
    mutations: {
        addReservation (state, reservation) {

            state.reservations.push(reservation)

        }
    },
    actions: {}
});